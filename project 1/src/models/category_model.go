package models

import (
	"database/sql"
	"entities"
	
)

type CategoryModel struct {
	Db *sql.DB
}

func (categoryModel CategoryModel) FindAll() (category []entities.Category,err error) {
	rows, err := categoryModel.Db.Query("select * from category")
	if err != nil {
		return nil, err
	} else {
		var categorys []entities.Category
		for rows.Next() {
			var id int64
			var main_category string
			var sub_category string
			var product_name string
			var price float64
			var quantity int64
			var description string
			err2 := rows.Scan(&id, &main_category, &sub_category, &product_name, &price, &quantity, &description)
			if err2 != nil {
				return nil, err2
			} else {
				category := entities.Category {
					Id: id,
				    Main_Category: main_category,
			        Sub_Category: sub_category,
			        Product_Name: product_name,
					Price: price,
					Quantity: quantity,
					Description: description,
				}
				categorys = append(categorys, category)
			}
		}
		return categorys, nil
	}
}


func (categoryModel CategoryModel) Create(category *entities.Category) (err error) {
	result, err := categoryModel.Db.Exec("insert into category(main_category, sub_category, product_name, price, quantity, description)values(?, ?, ?, ?, ?, ?)", category.Main_Category, category.Sub_Category, category.Product_Name, category.Price, category.Quantity, category.Description)
	if err != nil {
		return err
	} else {
	 category.Id, _ = result.LastInsertId()
	 return nil
}
}

func (categoryModel CategoryModel) Update(category *entities.Category) (int64, error) {
	result, err := categoryModel.Db.Exec("update category set main_category= ?, sub_category= ?, product_name = ?, price = ?, quantity = ?, description= ? where id = ?",  category.Main_Category, category.Sub_Category, category.Product_Name, category.Price, category.Quantity, category.Description, category.Id)
	if err != nil {
		return  0, err
	} else {
	
	 return result.RowsAffected()
}
}
func (categoryModel CategoryModel) Delete(id int64) (int64, error) {
	result, err := categoryModel.Db.Exec("delete from category where id= ?", id)
	if err != nil {
		return  0, err
	} else {
	
	 return result.RowsAffected()
}
}



