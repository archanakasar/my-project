package entities

import "fmt"

type Category struct {
	Id       int64  `json:"id"`
	Main_Category  string  `json:"main_category"`
	Sub_Category  string  `json:"sub_category"`
	Product_Name     string  `json:"product_name"`
	Price    float64  `json:"price"`
	Quantity int64   `json:"quantity"`
	Description string  `json:"description"`
}

func (category Category) ToString() string {
	return fmt.Sprintf("id: %d\nmain_category: %s\nsub_category: %s\nproduct_name: %s\nprice: %0.1f\nquantity: %d\ndescription: %s", category.Id, category.Main_Category, category.Sub_Category, category.Product_Name, category.Price, category.Quantity, category.Description)	
}   