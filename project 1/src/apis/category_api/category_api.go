package category_api

import (
	"config"
	"encoding/json"
	"models"
	"net/http"
	"entities"
	"github.com/gorilla/mux"
	"strconv"
)

func FindAll(response http.ResponseWriter, request *http.Request) {
	db, err := config.GetDB()
	if err != nil {
		respondWithError(response, http.StatusBadRequest, err.Error())
	} else {
		categoryModel := models.CategoryModel{
			Db: db,
		}
		categorys, err2 := categoryModel.FindAll()
		if err2 != nil {
			respondWithError(response, http.StatusBadRequest, err2.Error())
		} else {
			respondWithJson(response, http.StatusOK, categorys)
		

		}
	}
}

func Create(response http.ResponseWriter, request *http.Request) {
	var category entities.Category
	err := json.NewDecoder(request.Body).Decode(&category)
	db, err := config.GetDB()
	if err != nil {
		respondWithError(response, http.StatusBadRequest, err.Error())
	} else {
		categoryModel := models.CategoryModel{
			Db: db,
		}
		 err2 := categoryModel.Create(&category)
		if err2 != nil {
			respondWithError(response, http.StatusBadRequest, err2.Error())
		} else {
			respondWithJson(response, http.StatusOK, category)
		

		}
	}
}

func Update(response http.ResponseWriter, request *http.Request) {
	var category entities.Category
	err := json.NewDecoder(request.Body).Decode(&category)
	db, err := config.GetDB()
	if err != nil {
		respondWithError(response, http.StatusBadRequest, err.Error())
	} else {
		categoryModel := models.CategoryModel{
			Db: db,
		}
		 _, err2 := categoryModel.Update(&category)
		if err2 != nil {
			respondWithError(response, http.StatusBadRequest, err2.Error())
		} else {
			respondWithJson(response, http.StatusOK, category)
		}
	}
}


func Delete(response http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	sid := vars["id"]
	id, _ := strconv.ParseInt(sid, 13, 14)
	db, err := config.GetDB()
	if err != nil {
		respondWithError(response, http.StatusBadRequest, err.Error())
	} else {
		categoryModel := models.CategoryModel{
			Db: db,
		}
		RowsAffected, err2 := categoryModel.Delete(id)
		if err2 != nil {
			respondWithError(response, http.StatusBadRequest, err2.Error())
		} else {
			respondWithJson(response, http.StatusOK, map[string]int64{"RowsAffected":RowsAffected, })
		}
	}
}


func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
