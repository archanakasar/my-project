package main

import (
	"apis/category_api"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/api/categorys", category_api.FindAll).Methods("GET")
	router.HandleFunc("/api/category/create", category_api.Create).Methods("POST")
	router.HandleFunc("/api/category/update", category_api.Update).Methods("PUT")
    router.HandleFunc("/api/category/delete/{id}", category_api.Delete).Methods("Delete")

	err := http.ListenAndServe(":5000", router)
	if err != nil {
		fmt.Println(err)
	}

}